import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { ProdutoRequest } from '../models/produto-request';
import { ApiRestService } from './api-rest.service';

import { ItemCodigo } from '../models/itemCodigo';
import { RegraFiscal } from '../models/regrasFiscais';
import { Builder } from 'builder-pattern';


@Injectable()
export class ApiServiceService {

  constructor(private apiRestService: ApiRestService) { }

  

  public buscaItemCodigo(codigoDoitem: number): Observable<any> {
       return this.apiRestService.buscaItemCodigo(this.criarProdutoRequest(codigoDoitem))

  }

  private criarProdutoRequest(codigoDoItem: number) {
    const itemCodigo: ItemCodigo = Builder<ItemCodigo>()
      .codigo(codigoDoItem)
      .quantidade(1)
      .build();
    const consultaRegrasFiscais: RegraFiscal = Builder<RegraFiscal>()
      .pais('BR')
      .paisDestino('BR')
      .uf('RS')
      .ufDestino('RS')
      .build()

    return Builder<ProdutoRequest>()
      .filial(101)
      .itens([itemCodigo])
      .consultaRegrasFiscais(consultaRegrasFiscais)
      .perfil(1)
      .build();
  }
}
