import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { ProdutoRequest } from '../models/produto-request';

@Injectable()
export class ApiRestService {

    constructor(private http: HttpClient) { }


    public buscaItemCodigo(produtoRequest: ProdutoRequest): Observable<any> {
        return this.http.post<any>
            ('http://tst.grupodimedservices.com.br/mostruario/v3/itens/detalhe?filter=default', produtoRequest)

    }
 


}