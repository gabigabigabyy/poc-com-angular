import { ItemCodigo } from './itemCodigo';
import { RegraFiscal } from './regrasFiscais'
export class ProdutoRequest {
  
  constructor(
    public filial: number,
    public perfil: number,
    public itens: ItemCodigo[],
    public consultaRegrasFiscais: RegraFiscal
  ) { }

}

