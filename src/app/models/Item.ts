export class Item{
    codigo:number;
            codigoFilial:number;
            precoDe:number;
            precoPor:number;
            quantidade:number;
            percentualDesconto: number;
            descricaoDetalhadaItem: string; 
            codigoFabricante: number;
            ean:number;
            categoria:string;
            participaPackNovo: boolean;
            participaPbm: boolean;
            regraFiscal:string;
            codigoLegenda: number;
            codigoCfop: number;
            codigoSituacaoTributaria: number;
            aliquotaIcms: number;
            percentualIsencao: number;
            somaIsencaoComPromocao: boolean;
           
           qtdApresentacao: number;
            qtdApresentacaoUnitaria: number;
            codigoPrincipioAtivo: number;
            principioAtivo: string;
            codigoTipoDoItem: number;
            vendaLiberada: boolean
            psicotropico: boolean
            retencaoReceita: boolean
            possuiGenericos: boolean
            lancamento: boolean
            possuiSimilares: boolean
            possuiAlternativos: boolean
            possuiSimilaresPorPrincipioAtivo: boolean
            possuiGenericosPorPrincipioAtivo: boolean
            possuiReferenciaPorPrincipioAtivo: boolean
            possuiItemAVencer: boolean
            bloqueadoInternet: boolean
            presenteavel: boolean
            permiteAdesao: boolean
            possuiKitAdesao: boolean
            exclusivoPanvel: boolean
            participaListaReferencial: boolean
            participaFarmaciaPopular: boolean
            promocaoAssinatura: boolean
            itemGeladeira: boolean
            possuiEanImpresso: boolean
            usoContinuo: boolean
            farmaciaPopular: boolean
            multiAtendimento: boolean
            geraDadosFornecedor: boolean
            itemAssinavel: boolean
            situacaoItem: string;
            advertencias: string[]; //nao criar
            // [
            //                     A persistirem os sintomas, o médico deverá ser consultado.
            // ],
            nomenclatura: string;
            nomenclaturaDetalhada: string
            // dadosImagens: [
            //     {
            //         sequenciaImagem: 37157,
            //         numeroImagem: 1,
            //         imagemPreferencial: S,
            //         url: http://staticpanvel.com.br/produtos/15/113831-15.jpg
            //     }
            // ],
            // linksVideo: [],
            // restricoes: [],
            // indicadorOtc: S,
            // codigoMarcaPai: 767,
            // descricaoMarcaPai: DORFLEX,
            // reposicao: Sim,
            // dataPrimeiraEntrada: 03/12/2019,
            // dataUltimaVenda: 13/04/2021,
            // dataUltTransRecebidaDimed: 10/06/2020,
            // custoMedio: 15.13,
            // precoBaseZero: 16.08,
            // percentualMargemBruta: 32.12,
            // percentualMargemLiquida: 32.12,
            // fatorMargem: 99.99,
            // custoReposicao: 12.71

            // nRest:any;
        }

