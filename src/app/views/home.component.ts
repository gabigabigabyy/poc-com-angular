import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { FormControl, FormGroup } from '@angular/forms';



export class ItemModel {
    codigo: number;
    nomenclatura: string;
    precoDe: number;
    
}



@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']

})



export class HomeComponent implements OnInit {


    total: number = 23.50;
    quantity: number = 1;
    valor: number = 10;
    valorUnitario: number = 23.50
    formatacao: string = ""
    item: ItemModel
    notFound:boolean=false


    constructor(private apiservice: ApiServiceService) { }


    buscarForm = new FormGroup({ codigoItem: new FormControl('') });

    ngOnInit(): void {
        
        

            }

    onSubmit() {
        
             this.apiservice.buscaItemCodigo(this.buscarForm.value.codigoItem).subscribe(data => {
            console.log(data);

            if(data.itens.length>0){

            let item = new ItemModel()
            item.codigo = data.itens[0].codigo
            item.nomenclatura = data.itens[0].nomenclatura
            item.precoDe = data.itens[0].precoDe

            this.item = item
            console.log(this.item)

            this.formatacao = this.valorUnitario.toFixed(2).replace(".", ",");
            this.notFound=false

            }
            else{
                this.notFound=true
            }

            console.log(data)
        })
                 
                 
        
    }

    i = 1
    plus() {
        if (this.i != 99) {
            this.i++;
            this.quantity = this.i;
        }

        this.total = this.valorUnitario * this.quantity;
        this.formatacao = this.total.toFixed(2).replace(".", ",")
        console.log(this.valorUnitario)
        console.log(this.quantity)
        console.log(this.total)
        console.log(this.formatacao)


    }
    minus() {

        if (this.i != 1) {
            this.total = ((this.quantity - 1) * this.valorUnitario)
            this.formatacao = this.total.toFixed(2).replace(".", ",")
            this.i--;
            this.quantity = this.i;
        }
    }

    clear() {
        this.item = undefined
    }



}
