import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule} from './app.routing.module';
import {HomeComponent} from './views/home.component';
import { ApiServiceService } from './services/api-service.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { ApiRestService } from './services/api-rest.service';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    

    
    
  ],
  providers: [ApiServiceService, ApiRestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
